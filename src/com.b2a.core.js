

com = {
  b2a: {
    keys : {
      visible : "b2a_visible",
      collection : "b2a_collection",
      overflow : "b2a_overflow",
      
      width : "b2a_width",
      height : "b2a_height",
      styleWidth : "b2a_style_width",
      styleHeight : "b2a_style_height",
      padding : "b2a_padding",
      margin : "b2a_margin",
      fontSize : "b2a_fontSize",
      position : "b2a_position"
    },
    
    filters : {
      hulu : function(item) { 
        var prefix = "http://www.hulu.com/";
        return item.src.substring(0, prefix.length) === prefix;
      }
    },
    
    elements : function() {
      a = [
          { name: "object", filter : null },
          { name: "video", filter : null },
          { name: "embed", filter : null },
          { name: "iframe", filter : com.b2a.filters.hulu }
        ];
      return a;
    },
    
    string : {
      startsWith : function(text, search)
      {
        return text.slice(0, search.length) == search;
      },
      
      endsWith : function(text, search)
      {
        return text.slice(-search.length) == search;
      }
    },
    
    log: function(text) {
      // console.log(text);  
    },
    
    logError: function(text) {
      console.log(text);  
    },
    
    array_contains: function(haystack, needle)
    {
      for(var i = 0; i < haystack.length; i++)
      {
        if(this[i] === needle)
        {
          return i;
        }
      }
      
      return -1;
    },
    
    isElement : function(obj)
    {
      //com.b2a.log("found (on adding): " + obj + " with NodeType: " + obj.nodeType);
      return obj.nodeType == 1;
    },
    
    isTextNode : function(obj)
    {
      //com.b2a.log("found (on adding): " + obj + " with NodeType: " + obj.nodeType);
      return obj.nodeType == 3;
    },
    
    isShit : function(element)
    {
      com.b2a.log("is Shit: " + element);
      var i=0;
      var comparisonItems = com.b2a.elements();
      for (i = 0; i < comparisonItems.length; i++)
      {
        var target = comparisonItems[i];
        //com.b2a.log("checking for: " + target.name + " with el: " + element );
        if (element.tagName.toLowerCase() == target.name)
        {
          if (target.filter == null)
          {
            return true;
          }
          else
          {
            try
            {
              return target.filter(element);
            }
            catch(err)
            {
              com.b2a.logError(err);
            }
          }
        }
      }
      
      return false;
    },
  
    aggShit : function(collection, tagname, filter, isClean)
    {
      var coll = document.getElementsByTagName(tagname);
      var i = 0;
      for(i=0;i<coll.length;i++)
      {
        var item = coll[i];
        if (filter == null || filter(item))
        {
          if (!isClean)
          {
            collection.push(item);
          }
          else if(item.hasAttribute(com.b2a.keys.fontSize))
          {
            collection.push(item);
          }
        }
      }
      
      com.b2a.log("Found '" + tagname + "' count: " + i);
    },
    
    hasShit : function(items, isClean)
    {
      if (items == null)
      {
        items = [];
      }
      
      var i=0;
      var comparisonItems = com.b2a.elements();
      for (i = 0; i < comparisonItems.length; i++)
      {
        var target = comparisonItems[i];
        com.b2a.aggShit(items, target.name, target.filter, isClean);
      }
      
      return items.length == 1;
    },
    
    runShitCleaner : function(coll)
    {
      var items = [];
      this.hasShit(items, false);
      this.runShitCleanerFor(items);
    },
    
    runShitCleanerFor : function(coll)
    {
      var stripper = new com.b2a.Stripper();
      var putter = new com.b2a.Putter();
      
      com.b2a.ui.setToWindowSize();
      document.body.onkeyup = null;
      
      if (document.body.hasAttribute(com.b2a.keys.visible))
      {
        var items = []
        com.b2a.hasShit(items, true);
        if (coll.length != items.length)
        {
          chrome.extension.sendRequest({enable:false}, function(response) {});
        }
        putter.doAllShit(items);
      }
      else
      {
        com.b2a.log("Stripping...");
        
        if (coll.length > 1)
        {
          com.b2a.log("Cancelling Stripping...");
          return;
        }
        
        var items = stripper.doAllShit(coll);
        var i = 0;
        for (i = 0; i < items.length; i++)
        {
          var item = items[i];
          var height = window.getComputedStyle(item).height;
          if (height == "0px")
          {
            com.b2a.ui.setToWindowSize(i + 1);
          }
          var height = window.getComputedStyle(item).height;
          if (height == "150px")
          {
            com.b2a.ui.setToWindowSize(i + 1);
          }
        }
        
        document.body.onkeyup = function(event)
        {
          if (event.keyCode == 27)
          {
            com.b2a.runShitCleaner();
          }
        }
      }
    },
    repeatFor: function(check, func, timeout, max, count)
    {
      com.b2a.log("max: " + max + " count: "  + count);
      if (count >= max)
      {
        return;
      }
      
      if(check())
      {
        func();
      }
      else
      {
        setTimeout(function() { com.b2a.repeatFor(check, func, timeout, max, count + 1); }, timeout);
      }
    }
  }
}
