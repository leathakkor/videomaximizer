

com.b2a["Stripper"] = function()
{
  this.handleBodyStyling = function()
  {
    document.body.setAttribute(com.b2a.keys.overflow, document.body.style.overflow);
    document.body.style.overflow = "hidden";
    document.body.style.minHeight = "50px";
  };
  
  this.handleCompletedOfItem = function(el)
  {
    // SUPPORT FOR NOVAMOV.COM
    // AND OTHER FLASH SITES!
    if (window.location.host == "www.novamov.com")
    {
      try{ el.Zoom(100);el.Zoom(0); } catch(err) { }
    }
    // 
    var cn =  document.body.childNodes;
    for(i=0;i<cn.length;i++)
    {
      var currItem = cn[i];
      if (currItem.style != null)
      {
        var opacity = parseFloat(currItem.style.opacity);
        if (0.95 > opacity && opacity > .5)
        {
          currItem.style.zIndex = "-300";
        }
      }
    }
  }; 
  
  this.handleChildTextStyling = function(el)
  {
  };
  
  this.handleElementStyling = function(el)
  {
    //PRESERVE
    el.setAttribute(com.b2a.keys.width, el.width);
    el.setAttribute(com.b2a.keys.height, el.height);
    el.setAttribute(com.b2a.keys.styleWidth, el.style.width);
    el.setAttribute(com.b2a.keys.styleHeight, el.style.height);
    
    el.setAttribute(com.b2a.keys.padding, el.style.padding);
    el.setAttribute(com.b2a.keys.margin, el.style.margin);
    el.setAttribute(com.b2a.keys.fontSize, el.style.fontSize);
    el.setAttribute(com.b2a.keys.position, el.style.position);
    
    el.setAttribute(com.b2a.keys.visible, el.style.display);
    
    // UPDATE
    el.width="100%";
    el.height="100%";
    el.style.width="100%";
    el.style.height="100%";
  
    el.style.padding = "0 0 0 0";
    el.style.margin = "0 0 0 0";
    el.style.fontSize = "0px";
    el.style.position = null;
    
    el.style.display = null;
  }
  
  this.handleChildElementStyling = function(item)
  {
    //&& item.getAttribute(com.b2a.key)=="true"
    if (item.hasAttribute(com.b2a.keys.visible))
    {
      //com.b2a.log("Item already turned" + item);
    }
    else
    {
      item.setAttribute(com.b2a.keys.visible, item.style.display);
      item.style.display = "none";
    }
  }
}

com.b2a.Stripper.prototype = com.b2a.RecurserBase;
